Benchmarking CSPong
=========

This project forks from CSSamples, a repository that has a CSPong game that aides the user in understanding how ChilliSource works. 

This serves only as a way to stress some of the ChilliSource systems, Particle and UI, in order to benchmark and fix bottlenecks in them.

Note that the *automation* branch is the branch that is used to automatically benchmark the system, while the *master* branch is the branch that holds something that looks/feels more like a game and has an user interface.

Links
-----
* ChilliWorks [Website](http://chilli-works.com/)
* ChilliSource [Documentation](http://chilli-source.chilli-works.com/docs/)
* ChilliSource [Repository](https://github.com/ChilliWorks/ChilliSource)